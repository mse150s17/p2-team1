import matplotlib.pyplot as plt
#This code installs the necessary program to create plots from data
import numpy as np
#This is an extension of python with various scientific graphing methods
import sys
#This package allows us the ability to take the array that was created from numpy.loadtxt and place it in an Excel Spreadsheet.
#Module that allows the creation of a data array
import xlsxwriter

#This line of data uses the oneth argument as the file to execute this program on.
#Inserted usecols=(2,3) so we can isolate Load and Stress.
data = np.loadtxt(sys.argv[1],comments='"',delimiter=',',usecols=(2,3))

#Added the comments and delimiter in order to decipher the data sets and pull only the data and not the headings.
dataLength = len(data)
#This turns dataLength (originally and integer) into a string 
dataLength = str(dataLength)

#This defines x and y as the two columns in our data (Stress vs Load)
x = data[:,1]
y = data[:,0]

#This line obtains the argument file specified by user and replaces the .raw extension with the .xlsx expansion. 
workbookName = sys.argv[1].replace(".raw",".xlsx")

#Named the Workbook with workbookName string created above. 
workbook = xlsxwriter.Workbook(workbookName)

worksheet = workbook.add_worksheet()

#instantiated two variables for the number of rows and columns in our excel table
row = 0 
col = 0

#Here the code tells the computer to place all the data in the first two columns.
for extension, stress in (data):
		worksheet.write(row, col,    extension)
		worksheet.write(row, col +1, stress)
		row +=1

#This code adds the chart/graph to the excel. Then it tells it where to get the 'categories' and 'values' of (x.y).
chart = workbook.add_chart({'type': 'scatter'})
chart.add_series({
		'categories':'=Sheet1!$A$1:$A$'+dataLength,
		'values':'=Sheet1!$B$1:$B$'+dataLength,
		'trendline': {'type': 'linear',
				'display_equation': True,
				'display_r_squared': True,
				'line': {
					'color': 'blue',
					'width': 1,
					'dash_type': 'long_dash',
				},
			},
})

chart.set_title({'name':'Stress vs Extension',})
chart.set_y_axis({'name': 'Stress',})
chart.set_x_axis({'name':'Extension',})

worksheet.insert_chart('C1', chart)
workbook.close()
