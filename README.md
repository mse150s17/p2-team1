# README #

# Project 2

* This repository has some template data files and starter code for our Spring 2017 project #2

Summary of set up: 

We created two files that will organize data from a text file into an excel graph using numpy, sys, and xlsxwriter. It will currently read in the data file into a numpy array, and verify it has done so as expected by printing it out. The only difference between the two ".py" programs is that the delimiter was changed from " " to "," for the bending data, because it was organized differently in the text files.

* Configuration

The data is extrapolated from the file, specified in the arguments, by using the loadtxt() function in the numpy library. This will create an array with the numerical data, with separated stress and extension values. By importing the xslxwriter library, we can create and name a workbook using the xlsxwriter.Workbook(str filename) function. A 'for' loop is then used to populate the cells in the excel sheet. The add_chart() function is used to create a scatter plot in the Excel file. To populate and edit the chart, the add_series function is used. After settings, chart titles, and axes names, the chart was inserted into the worksheet. When ran properly, the code should return a "Data_Analysis.xlsx" file within the repository. Every time the code is ran, the code should create a .xlsx file within the repository. If the python script is ran for multiple data files, it will not overwrite the previous Excel files unless the data files are identical.

* Dependencies

"plot_bending.py" will only work if the strain values are in the 2nd column, and the stress values in the 3rd column of the raw data file. This is because "plot_bending.py" was made specifically for the format of the bending data files. If stress-strain data from another program was collected, it would work with the code, as long as the data files or code are formatted accordingly. Similarly, "plot_spectra.py" will only work if the wavelength is in the 0th column, and peak intensity is in the 1st column. This is done because the format of the raw spectra data differs from the raw bending data.

* Database configuration
* Deployment instructions

The current example code can be invoked with:

$ python plot_bending.py (name of directory of datafile) (name of bending data file) when creating an Excel graph for the bending data

$ python plot_spectra.py (name of directory of datafile)(name of spectra data file) when creating an Excel graph for the spectra data

Example: $ python plot_bending.py bending/Sp15_245L_sec-001_group-01_bendtest-aluminum.raw

The above prompts will each produce an Excel file with the same name as the raw data file. This Excel file will be located in the same directory as the raw data file (i.e. bending or spectra) and contains the 2 columns of data necessary to construct a plot. Once open, the Excel files will produce graphs of Stress vs Strain for the bending data, and wavelength absorption from the spectrum data.

If the file doesn't automatically open using an excel file reader, it may be necessary to right click the file and open the file with one.
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* If there are any questions about the code you can contact anyone in the contributers.txt file.

* Repo owner or admin:Timothy Wanless, Carter Warren, Tyler Webb, Paige Skinner, Riccardo Torsi, Dallas Holstine, Kylee Lay

* Other community or team contact: Boise State University