import matplotlib.pyplot as plt
#This code installs the necessary program to create plots from data
import numpy as np
#This line imports numpy library so we can manipulate our data with math programs

import sys
#This imports sys library that will allows us to call on the different arguments 

import xlsxwriter
#Xlsxwriter is a library that will allow us to write an excel file.

data = np.loadtxt(sys.argv[1])
#This line of data uses the oneth argumentas the file to execute this program on.
dataLength = len(data)
#This variable equals the length of the data points.
dataLength = str(dataLength)

# Since the default deliminter is any white space in the np.loadtxt(), we don't need any delimiter or specify which columns to use because the file only has two columns.
x = data[:,0]
y = data[:,1]

#Setting variables x and y gives us the ability to select the data in our array created by np.loadtxt

#This line obtains the argument file specified by user and replaces the .raw extension with the .xlsx expansion. 
if sys.argv[1].find("raw") == 1:
        workbookName = sys.argv[1].replace(".raw",".xlsx")
else:
        workbookName = sys.argv[1]+".xlsx"

#Named workbook with workbookName varible created above. 
workbook = xlsxwriter.Workbook(workbookName)

#created a varible workbook and assigning to xlsxwriter.Workbook and naming the spreadsheet Spectrum_Analysis.xlxs

worksheet = workbook.add_worksheet()

#placing data in the worksheet

row = 0
col = 0

# These two lines specify where to start placing the data in the worksheet.

for x, y in (data):
	worksheet.write(row, col, x)
	worksheet.write(row, col +1, y)
	row +=1

chart = workbook.add_chart({'type': 'line'})
chart.add_series({
                'categories':'=Sheet1!$A$1:$A$'+dataLength,
                'values':'=Sheet1!$B$1:$B$'+dataLength,
                                },
)

chart.set_size({'width': 1000, 'height': 576})
chart.set_title({'name':'Absorbance vs Wavelength',})
chart.set_y_axis({'name': 'Absorbance',})
chart.set_x_axis({'name':'Wavelength',})

worksheet.insert_chart('C1', chart)
workbook.close()
